require "rails_helper"

RSpec.feature "Users signup" do

	scenario "with valid credentials" do
		visit "/"

		fill_in "First Name", with: "Tom"
		fill_in "Last Name", 	with: "Tester"
		fill_in "Username", 	with: "TomTester"
		within(:css, ".form") do
			fill_in "Email Address", 			with: "TomTester@testuser.com"
		end
				
		fill_in "Password", 	with: "A1B2B3E5D4!@"
		click_button "Join Now"

		expect(page).to have_content("Welcome")
		expect(page.current_path).to eq(welcome_path)
	end

end
