require 'spec_helper'

RSpec.feature "Creating Posts" do

	scenario "A user creates a new post" do

		visit "/activities"

		#save_and_open_page
		#first(:button, '+ Add Post').click
		#find_button('+ Add Post').click
		page.has_button?('Add Post')

		fill_in "field", with: "This is a prayer request."

		click_button "Submit Post"

		expect(page).to have_content("Post Created")
		expect(page.current_path).to(activities_path)

	end
	
end
