class AddSubscriberAndStripeIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :subscribed, :boolean, null: false, default: false
    add_column :users, :subscription_plan, :string
    add_column :users, :stripe_id, :string
  end
end
