class CreateGroupPosts < ActiveRecord::Migration
  def change
    create_table :group_posts do |t|
      t.references :user, index: true
      t.references :group, index: true
      t.text :content
      t.string :post_type

      t.timestamps null: false
    end
  end
end
