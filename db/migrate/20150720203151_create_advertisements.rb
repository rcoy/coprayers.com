class CreateAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements do |t|
      t.string :title
      t.text :content
      t.string :link_text
      t.string :link

      t.timestamps null: false
    end
  end
end
