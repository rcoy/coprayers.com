# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(username: "richcoy", email:"rich.a.coy@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Richard", last_name: "Coy", city:"Orlando", state:"Florida", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now, admin: true)
User.create(username: "conniecoy", email:"connie.coy@cru.org", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Connie", last_name: "Coy", city:"Orlando", state:"Florida", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test1", email:"test1@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Ben", last_name: "Johnson", city:"Chicago", state:"Illinois", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test2", email:"test2@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Shari", last_name: "Jackson", city:"Las Vegas", state:"Nevada", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test3", email:"test3@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Ileen", last_name: "McMillan", city:"Miami", state:"Florida", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test4", email:"test4@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Braden", last_name: "Allen", city:"Orlando", state:"Florida", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test5", email:"test5@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Abigail", last_name: "Coy", city:"Orlando", state:"Florida", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test6", email:"test6@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Max", last_name: "Harrison", city:"Boston", state:"Mass", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test7", email:"test7@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Ben", last_name: "Carilson", city:"Washington", state:"DC", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test8", email:"test8@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Anna", last_name: "Boisdorf", city:"Ovideo", state:"Florida", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test9", email:"test9@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Jonathan", last_name: "Brown", city:"Dallas", state:"Texas", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)
User.create(username: "test10", email:"test10@gmail.com", password:"Josh2415!@#", password_confirmation:"Josh2415!@#", first_name: "Jose", last_name: "Canusee", city:"San Diego", state:"Califorina", bio:"Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.", confirmed_at: Time.now)

p "12 Test Users Created"

Friendship.create(user_id: "1", friend_id: "2", state: "active", friended_at: Time.now)
Friendship.create(user_id: "1", friend_id: "3", state: "active", friended_at: Time.now)
Friendship.create(user_id: "1", friend_id: "4", state: "active", friended_at: Time.now)
Friendship.create(user_id: "5", friend_id: "1", state: "pending")
Friendship.create(user_id: "1", friend_id: "6", state: "pending")
Friendship.create(user_id: "2", friend_id: "3", state: "active", friended_at: Time.now)
Friendship.create(user_id: "2", friend_id: "5", state: "active", friended_at: Time.now)

p "7 Friendships Created"

Post.create(user_id: "1", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa.")
Post.create(user_id: "2", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.")
Post.create(user_id: "3", post_type: "praise", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa.")
Post.create(user_id: "4", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim.")
Post.create(user_id: "2", post_type: "praise", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa.")
Post.create(user_id: "1", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder.")
Post.create(user_id: "4", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.")
Post.create(user_id: "3", post_type: "praise", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin.")
Post.create(user_id: "1", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa.")
Post.create(user_id: "2", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.")
Post.create(user_id: "2", post_type: "praise", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa.")
Post.create(user_id: "3", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim.")
Post.create(user_id: "2", post_type: "praise", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa.")
Post.create(user_id: "1", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder.")
Post.create(user_id: "2", post_type: "prayer", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim pastrami frankfurter beef ribs shoulder prosciutto, short ribs alcatra.")
Post.create(user_id: "4", post_type: "praise", content: "Bacon ipsum dolor amet filet mignon shoulder porchetta shankle picanha cupim boudin prosciutto ham hock kielbasa. Ground round cupim.")


p "16 Test Posts Created"

Advertisement.create(title: "Mere Christianity", content: "This is C.S. Lewis's best book ever.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Best of Mercyme", content: "This Christian band rocks and this is there best hits all on one CD.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 3", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 4", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 5", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 6", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 7", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 8", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 9", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")
Advertisement.create(title: "Advertisement 10", content: "This is some text for this advertisement. Buy this thing.", link_text: "See it on Amazon Now", link: "http://www.Amazon.com")

p "10 Advertisements Created"


