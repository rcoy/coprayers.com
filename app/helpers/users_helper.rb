module UsersHelper

	def action_buttons(user)
		case current_user.friendship_status(user) when "friends"
			link_to(friendship_path(current_user.friendship_relation(user)), method: :delete, :remote => true, data: {disable_with: "<i class='fa fa-spinner fa-spin'></i> Canceling"}, class: "js-cancel-friendship-btn btn btn-danger btn-sm") do content_tag(:i, "", class: "fa fa-times fa-fw", style: "color: #ffffff;") + " Cancel Friendship" end
		when "pending"
			link_to(friendship_path(current_user.friendship_relation(user)), method: :delete, :remote => true, data: {disable_with: "<i class='fa fa-spinner fa-spin'></i> Canceling"}, class: "js-cancel-request-btn btn btn-danger btn-sm") do content_tag(:i, "", class: "fa fa-times fa-fw", style: "color: #ffffff;") + " Cancel Request" end
		when "requested"
			link_to(accept_friendship_path(current_user.friendship_relation(user)), method: :put, :remote => true, data: {disable_with: "<i class='fa fa-spinner fa-spin'></i> Accepting"}, class: "js-accept-friendship-btn btn btn-primary btn-sm")  do content_tag(:i, "", class: "fa fa-plus fa-fw", style: "color: #ffffff;") + " Accept Friendship" end +
			link_to(friendship_path(current_user.friendship_relation(user)), method: :delete, :remote => true, data: {disable_with: "<i class='fa fa-spinner fa-spin'></i> Declining"}, class: "js-decline-friendship-btn btn btn-danger btn-sm m-l-sm") do content_tag(:i, "", class: "fa fa-times fa-fw", style: "color: #ffffff;") + " Decline" end
		when "not_friends"
			link_to(friendships_path(user_id: user.id), method: :post, :remote => true, data: {disable_with: "<i class='fa fa-spinner fa-spin'></i> Requesting"}, class: "js-not-friend-btn btn btn-primary btn-sm") do content_tag(:i, "", class: "fa fa-plus fa-fw", style: "color: #ffffff;") + " Send Friend Request" end
		end
	end

	def action_buttons_profile(user)
		case current_user.friendship_status(user) when "friends"
			link_to "Cancel Friendship", friendship_path(current_user.friendship_relation(user)), method: :delete, :remote => true,  class: "js-profile-cancel-friendship-btn mr10"
		when "pending"
			link_to "Cancel Request", friendship_path(current_user.friendship_relation(user)), method: :delete, :remote => true, class: "js-profile-cancel-request-btn mr10"
		when "requested"
			link_to("Accept Friendship", accept_friendship_path(current_user.friendship_relation(user)), method: :put, :remote => true, class: "js-profile-accept-friendship-btn btn btn-primary btn-xs") +
			link_to("Decline", friendship_path(current_user.friendship_relation(user)), method: :delete, :remote => true, class: "js-profile-decline-friendship-btn btn btn-gold btn-xs")
		when "not_friends"
			link_to "Request Friendship", friendships_path(user_id: user.id), method: :post, :remote => true, class: "js-profile-not-friend-btn btn btn-primary btn-sm mr10"
		end
	end

end
