# == Schema Information
#
# Table name: subscriptions
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  subscribe_to_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Subscription < ActiveRecord::Base

	belongs_to :user
	belongs_to :subscribe_to, class_name: "User"

end
