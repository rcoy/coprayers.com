# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  username               :string           default(""), not null
#  first_name             :string
#  last_name              :string
#  city                   :string
#  state                  :string
#  bio                    :text
#  avatar                 :string
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  admin                  :boolean          default(FALSE)
#  status                 :string           default("active")
#

class User < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  after_update :crop_avatar

  acts_as_voter
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :async,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable, :zxcvbnable

  validates :first_name, :presence => true
  validates_format_of :first_name, :with => /\A[a-zA-Z]+\z/, :message => "only allows letters"
  validates :last_name, :presence => true
  validates_format_of :last_name, :with => /\A[a-zA-Z]+\z/, :message => "only allows letters"
  validates_presence_of :username
  validates_uniqueness_of :username
  validates_format_of :username, :with => /\A[a-zA-Z0-9]+\z/, :message => "only allows letters and numbers"

  has_many :friendships, dependent: :destroy
  has_many :inverse_friendships, class_name: "Friendship", foreign_key: "friend_id", dependent: :destroy

  has_many :subscriptions, dependent: :destroy
  has_many :subscribers, class_name: "Subscription", foreign_key: "subscribe_to_id", dependent: :destroy

  has_many :posts, dependent: :destroy
  has_many :group_posts, dependent: :destroy

  has_many :group_members
  has_many :groups, through: :group_members

  has_many :conversations

  include PgSearch
  pg_search_scope :search, against: [:first_name, :last_name, :city, :username]

  def request_friendship(user_2)
  	self.friendships.create(friend: user_2)
  end

  def pending_friend_requests_from
  	self.inverse_friendships.where(state: "pending")
  end

  def pending_friend_requests_to
  	self.friendships.where(state: "pending")
  end

  def active_friends
  	self.friendships.includes(:friend).where(state: "active").map(&:friend) + self.inverse_friendships.includes(:friend).where(state: "active").map(&:user)
  end

  def friendship_status(user_2)
    friendship = Friendship.where(user_id: [self.id,user_2.id], friend_id: [self.id,user_2.id])
    unless friendship.any?
      return "not_friends"
    else
      if friendship.first.state == "active"
        return "friends"
      else
        if friendship.first.user == self
          return "pending"
        else
          return "requested"
        end
      end
    end
  end


  def friendship_relation(user_2)
    Friendship.where(user_id: [self.id,user_2.id], friend_id: [self.id,user_2.id]).first
  end


# Subscriptions ======================

  def subscription_status(user_2)
    subscription = Subscription.where(user_id: self.id, subscribe_to_id: user_2.id)
    unless subscription.any?
      return "not_subscribed"
    else
      return "subscribed"
    end
  end

  def get_subscription(user_2)
    Subscription.where(user_id: self.id, subscribe_to_id: user_2.id).first
  end

  def subscribe_to(user_2)
    self.subscriptions.create(subscribe_to_id: user_2.id)
  end

  def subscribers
    Subscription.where(subscribe_to_id: self.id)
  end

# Groups ================================

  def admin_this_group?(group)
    GroupMember.where(user_id: id, group_id: group.id, role: 'admin').count > 0
  end

  def moderate_this_group?(group)
    GroupMember.where(user_id: id, group_id: group.id, role: 'admin').count > 0
  end

# Cropping ==============================

  def crop_avatar
    avatar.recreate_versions! if crop_x.present?
  end

# Search ================================

  def self.text_search(query)
    if query.present?
      search(query)
    else
      order("created_at DESC")
    end
  end

# Lock & Unlock =========================

  def lock_user
    self.update_attributes(status: "locked")
  end

  def unlock_user
    self.update_attributes(status: "active")
  end

# Unread Messages =======================

  def unread_messages
    
  end
    
end
