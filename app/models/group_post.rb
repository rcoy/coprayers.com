# == Schema Information
#
# Table name: group_posts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  group_id   :integer
#  content    :text
#  post_type  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GroupPost < ActiveRecord::Base

	acts_as_votable

	belongs_to :user
	belongs_to :group

	validates :user_id, :presence => true
	validates :content, :presence => true



end
