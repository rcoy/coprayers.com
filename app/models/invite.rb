class Invite

  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :email, :first_name

  validates :email,
    presence: true

end
