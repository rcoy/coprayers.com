# == Schema Information
#
# Table name: group_members
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  group_id     :integer
#  role         :string           default("pending")
#  email_notify :boolean          default(FALSE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class GroupMember < ActiveRecord::Base

  belongs_to :group
  belongs_to :user

  def make_member
  	self.update_attributes(role: "member")
  end

  def make_moderator
  	self.update_attributes(role: "moderator")
  end

  def make_admin
    self.update_attributes(role: "admin")
  end

  def reject_member
  	self.destroy
  end

  def remove_member
    self.destroy
  end

  def email_notify_on
    self.update_attributes(email_notify: true)
  end

  def email_notify_off
    self.update_attributes(email_notify: false)
  end
  
end
