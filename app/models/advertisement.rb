# == Schema Information
#
# Table name: advertisements
#
#  id         :integer          not null, primary key
#  title      :string
#  content    :text
#  link_text  :string
#  link       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Advertisement < ActiveRecord::Base
end
