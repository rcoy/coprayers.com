# == Schema Information
#
# Table name: groups
#
#  id           :integer          not null, primary key
#  name         :string
#  organization :string
#  location     :string
#  description  :text
#  privacy      :string
#  image        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Group < ActiveRecord::Base
	mount_uploader :image, GroupUploader
	attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  after_update :crop_image


	has_many :group_members, dependent: :destroy
	has_many :members, through: :group_members, source: :user

	has_many :group_posts, dependent: :destroy

	validates :name, :presence => { :message => " cannot be blank" }
	validates :name, length: {
    minimum: 5,
    maximum: 50,
    too_short: "must have at least %{count} characters",
    too_long: "must have at most %{count} characters"
  }

	include PgSearch
  pg_search_scope :search, against: [:name, :location, :organization]

	def admins
		members.includes(:group_members).where('group_members.role = ?', 'admin')
	end

	def moderators
		members.includes(:group_members).where('group_members.role = ?', 'moderator')
	end

	def regular_users
		members.includes(:group_members).where('group_members.role = ?', 'member')
	end

	def private?
		self.privacy == 'Private'
	end

	def subscribers
    group_members.where(email_notify: true)
  end

# Cropping ===========================

  def crop_image
    image.recreate_versions! if crop_x.present?
  end

# Search ===========================

def self.text_search(query)
	if query.present?
		search(query)
	else
		order("created_at DESC")
  end
end

end
