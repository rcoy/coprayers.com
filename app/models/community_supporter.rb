# == Schema Information
#
# Table name: community_supporters
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CommunitySupporter < ActiveRecord::Base
	belongs_to :user
end
