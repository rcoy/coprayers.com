class Contact

  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :name, :email, :reason, :content

  validates :name,
    presence: true

  validates :email,
    presence: true

  validates :content,
    presence: true,
    length: { maximum: 1000, too_long: "%{count} characters is the maximum allowed" }

end
