class ApplicationMailer < ActionMailer::Base

	default from: "CoPrayers.com <no-reply@coprayers.com>"
  layout 'mailer'

	# Friendship Emails ===========================================

	def friendship_requested(current_user,user)

		@current_user = current_user
		@recipient = user

		mail(	to: @recipient.email,
					subject: "CoPrayers Friendship Request From #{current_user.first_name + " " + current_user.last_name}",
				)
	end

	def friendship_accepted(current_user,user)

		@current_user = current_user
		@recipient = user

		mail(	to: @recipient.email,
					subject: "CoPrayers Friendship Accepted By #{current_user.first_name + " " + current_user.last_name}",
				)
	end

	# Group Invite Emails ==========================================

	def new_group_invite(current_user, group_invite, group)
  	@current_user = current_user
    @invite = group_invite
    @group = group
    
    mail( to: group_invite.email,
    	 		subject: "CoPrayers Group Invitation From #{current_user.first_name + " " + current_user.last_name}"
  	)
  end

  # Group Join Request Emails ===================================

  def join_requested(current_user, owner, group)

		@current_user = current_user
    @owner = owner
    @group = group
		
		mail(	to: owner.email,
					subject: "Group Join Request From #{current_user.first_name + " " + current_user.last_name}"
				)
	end

	def join_accepted(current_user, requestor, group)

		@current_user = current_user
    @requestor = requestor
    @group = group
		
		mail(	to: requestor.email,
					subject: "CoPrayers Group Join Accepted"
				)
	end

	def make_member_moderator(current_user, requestor, group)
		@current_user = current_user
    @requestor = requestor
    @group = group
		
		mail(	to: requestor.email,
					subject: "CoPrayers You've Been Promoted To Group Moderator"
				)
	end

	def make_member_admin(current_user, requestor, group)
		@current_user = current_user
    @requestor = requestor
    @group = group
		
		mail(	to: requestor.email,
					subject: "CoPrayers  You've Been Promoted To Group Administrator"
				)
	end

	# Group Post Emails To Group Subscribers =======================

	def send_group_prayer_request(current_user, group_post, subscriber)

		@current_user = current_user
		@group_post = group_post
    @user = subscriber
		
		mail(	to: @user.email,
					subject: "Group Prayer Request From #{current_user.first_name + " " + current_user.last_name}"
				)
	end

	# Site Invitation Emails ======================================

	def new_invite(current_user,invite)
  	@current_user = current_user
    @invite = invite
    
 		mail(	to: invite.email,
					subject: "Invitation from #{current_user.first_name + " " + current_user.last_name}"
				)
  end

  # Site Message Emails (Contact Form) =========================

  def new_contact(message)
    @message = message
    
    mail(	to: "support@coprayers.com",
    			from: message.email,
    		 	subject: "#{message.reason} Message from #{message.name}"
    		)
  end

  # Prayer Request Emails To Subscribers =========================

  def send_prayer_request(post, subscriber)
		@post = post
		@u = User.find(subscriber.user_id)
		mail(	:to => @u.email,
					:subject => "CoPrayers Post From #{@post.user.first_name} #{@post.user.last_name}"
				)
	end

	# Private Message Received Emails =============================

	def new_private_message(current_user,receiver,message)
  	@current_user = current_user
  	@receiver = receiver
  	@message = message
    
    mail( to: @receiver.email,
    	 		subject: "CoPrayers Private Message From #{current_user.first_name + " " + current_user.last_name}"
  	)
  end

end
