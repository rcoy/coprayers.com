/* -----------------------------------------------------------------

 	[Content Structure]

 	01. Helper Functions
	02. Website Enhancements and bug fixes
	03. Animations
	04. Ajax contact form
	05. CoPrayers Specific

------------------------------------------------------------------- */



(function($){
	"use strict";



/* *********************	Helper functions	********************* */


	/* Validate function */
	function validate(data, def) {
		return (data !== undefined) ? data : def;
	}

	var $win = $(window),

		// body 
		$body = $('body'),

		// Window width (without scrollbar)
		$windowWidth = $win.width(),

		// Media Query fix (outerWidth -- scrollbar) 
		// Media queries width include the scrollbar
		mqWidth = $win.outerWidth(true,true),

		// Detect Mobile Devices 
		isMobileDevice = (( navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone|IEMobile|Opera Mini|Mobi/i) || (mqWidth < 767) ) ? true : false );

		// detect IE browsers
		var ie = (function(){
		    var rv = 0,
		    	ua = window.navigator.userAgent,
		    	msie = ua.indexOf('MSIE '),
		    	trident = ua.indexOf('Trident/');

		    if (msie > 0) {
		        // IE 10 or older => return version number
		        rv = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
		    } else if (trident > 0) {
		        // IE 11 (or newer) => return version number
		        var rvNum = ua.indexOf('rv:');
		        rv = parseInt(ua.substring(rvNum + 3, ua.indexOf('.', rvNum)), 10);
		    }

		    return ((rv > 0) ? rv : 0);
		}());
	


/* *********************	Website enhancement & bug fixes	********************* */



	// Back to Top Button
	$body.append($('<div class="back-to-top"><i class="fa fa-angle-up"></i></div>'));

	$win.scroll(function(){
		if ($(this).scrollTop() > 1) {
			$('.back-to-top .fa').css({bottom:"0"});
		} else {
			$('.back-to-top .fa').css({bottom:"-70px"});
		}
	});

	$('.back-to-top .fa').click(function(){
		$('html, body').animate({scrollTop: '0'}, 500);
		return false;
	});



	// Pause or Play Video
	$("#video-button").on("click",function() {
		var $this = $(this),
			video = document.getElementById('video-fullwidth');

		if ($this.hasClass("pause")) {
			video.pause();
			$this.removeClass("pause").addClass("play");

		} else if ($this.hasClass("play")) {
			video.play();
			$this.removeClass("play").addClass("pause");
		}

		return false;
	});

	$("#video-fullwidth").css("height","100%");


	// Set a background overlay for revolution slider videos
	$win.load(function() {
		$(".html5vid").append($("<div class='bg-overlay op4' style='z-index:5'></div>"));
	});

	// Max Height 
	function max_height() {
		$(".max_height").each(function() {
			var maxHeight = 0;
			$(this).find(".el_max_height").each(function() {
				if ($(this).height() > maxHeight) {
		            maxHeight = $(this).height();
		        }
			}).height(maxHeight);
		});
	}

	$win.load(function() {
		max_height();
	});




	// Fix column sibling height 
	function fixHeight() {
		$(".data-height-fix").each(function() {
			var siblingHeight = $(this).find($(".get-height")).outerHeight();
			$(".set-height").css("height",siblingHeight);
		});
	}

	fixHeight();


	// Notifications 
	$("#show_notification").on("click",function() {
		$(".alert-modal").addClass('alert-modal-on');
		return false;
	});

	// Toggles upside-down 
	$(".panel-title").on("click","a",function() {
		$(this).find(".fa").toggleClass("upside-down");
	});


	// Body full height 
	function setWindowHeight() {
		var windowHeight = $(window).height();
		$(".window-fullheight").css("height",windowHeight);
	}

	setWindowHeight();



	// Fix height attribute on iframes
	$('iframe').each(function() {
		var $this = $(this);
		$this.css('height', $this.attr('height') + 'px');

	});

	/* Responsive Videos - 16:9 / 4:3 format */
	function rsEmbed() {
		$('.rs-video').each(function() {
			var $this = $(this),
				embedWidth = $this.width(),
				embedHeight = ( $this.hasClass('video-4by3') ? (embedWidth * 0.75) : (embedWidth * 0.5625) );

			$this.css('height', embedHeight + 'px');
		});
	}

	rsEmbed();


	// Fix IE9 placeholder 
	if (ie === 9) {
		$.getScript('../plugins/jquery.placeholder.js',function() {
			$('input, textarea').placeholder();
		});
	}




/* *********************	Animations	********************* */

	// Animations
	if ( ($().appear) && (isMobileDevice === false) ) {

		$('.animated').appear(function () {
			var $this = $(this);

			$this.each(function () {

				var animation = $this.data('animation'),
					delay = ($this.data('delay') + 'ms'),
					speed= ($this.data('speed') + 'ms');

				$this.addClass('on').addClass(animation).css({
					'-moz-animation-delay':delay,
					'-webkit-animation-delay':delay,
					'animation-delay':delay,
					'-webkit-animation-duration':speed,
					'animation-duration':speed
				});

			});
		}, {accX: 50, accY: -150});

	} else {

		$('.animated').removeClass("animated");
	}


	// Progress bars animations
	$(".progress").each(function() {

		var $this = $(this);

		if (($().appear) && (isMobileDevice === false) && ($this.hasClass("no-anim") === false) ) {	

			$this.appear(function () {

					var $bar = $this.find(".progress-bar");
					$bar.addClass("progress-bar-animate").css("width", $bar.attr("data-percentage") + "%");


			}, {accY: -150} );

		} else {

			var $bar = $this.find(".progress-bar");
			$bar.css("width", $bar.attr("data-percentage") + "%");
		}
	});



/* *********************	Ajax Contact Form	 ********************* */


	if ($('.ajax-contact-form').length ) {

		var form = {

			init: false,

			initialize: function() {

				// if form is already initialized, skip 
				if (this.init) { 
					return; 
				} 
				this.init = true;


				var $form = $(".ajax-contact-form");
			
				$form.validate({
					submitHandler: function(form) {

						// Loading Button
						var btn = $(this.submitButton);
						btn.button("loading");

						// Ajax Submit
						$.ajax({
							type: "POST",
							url: $form.attr("action"),
							data: {
								"val_fname": $form.find("#val_fname").val(),
								"val_lname": $form.find("#val_lname").val(),
								"g-recaptcha-response": $form.find('#g-recaptcha-response').val(),
								"val_email": $form.find("#val_email").val(),
								"val_subject":$form.find("#val_subject").val(),
								"val_message": $form.find("#val_message").val()
							},
							dataType: "json",
							success: function(data) {

								var $success = $form.find("#contact-success"),
									$error = $form.find("#contact-error"); 

								if (data.response == "success") {

									$success.removeClass("hidden");
									$error.addClass("hidden");

									// Reset Form
									$form.find(".form-control")
										.val("")
										.blur()
										.parent()
										.removeClass("has-success")
										.removeClass("has-error")
										.find("label.error")
										.remove();


								} else {

									$error.removeClass("hidden");
									$success.addClass("hidden");
								}
							},
							complete: function () {
								btn.button("reset");
							}
						});
					},
					rules: {
						val_fname: {
							required: false
						},
						val_lname: {
							required: false
						},
						val_email: {
							required: true,
							email: true
						},
						val_subject: {
							required: true
						},
						val_message: {
							required: true
						}
					},
					messages: {
						val_email: {
							required:"<span class='form-message-error'>Please enter your email address!</span>",
							email:"<span class='form-message-error'>Please enter a valid email address!</span>"
						},
						val_subject: {
							required:"<span class='form-message-error'>This field is required!</span>"
						},
						val_message: {
							required: "<span class='form-message-error'>This field is required!</span>"
						}
					},
					highlight: function (element) {
						$(element)
							.parent()
							.removeClass("has-success")
							.addClass("has-error");
					},
					success: function (element) {
						$(element)
							.parent()
							.removeClass("has-error")
							.addClass("has-success")
							.find("label.error")
							.remove();
					}
				});


			} // END initialize

		}; // END form object

		form.initialize();

	}

// Added for CoPrayers help popups
$(document).ready(function(){
    $('[data-toggle="popover"]').popover().click(function(event){event.preventDefault();})
});

// Added for CoPrayers tooltips
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

})(jQuery);
/* END Document ready */
