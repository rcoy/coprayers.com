# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  new ImageCropper()
class ImageCropper
  constructor: ->
    $('#group_cropbox').Jcrop(
      aspectRatio: 1
      setSelect: [0, 0, 500, 500]
      onSelect: @update
      onChange: @update
      )
  
  update: (coords) =>
    $('#group_crop_x').val(coords.x)
    $('#group_crop_y').val(coords.y)
    $('#group_crop_w').val(coords.w)
    $('#group_crop_h').val(coords.h)
    @updateGroupPreview(coords)

  updateGroupPreview: (coords) =>
    $('#group_preview').css
      width: Math.round(100/coords.w * $('#group_cropbox').width()) + 'px'
      height: Math.round(100/coords.h * $('#group_cropbox').height()) + 'px'
      marginLeft: '-' + Math.round(100/coords.w * coords.x) + 'px'
      marginTop: '-' + Math.round(100/coords.h * coords.y) + 'px'
