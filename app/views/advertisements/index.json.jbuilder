json.array!(@advertisements) do |advertisement|
  json.extract! advertisement, :id, :title, :content, :link_text, :link
  json.url advertisement_url(advertisement, format: :json)
end
