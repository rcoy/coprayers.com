class ContactController < ApplicationController

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    
    if @contact.valid?
      #ApplicationMailer.delay.new_contact(@contact)
      ApplicationMailer.new_contact(@contact)
      redirect_to contact_path, notice: "Your messages has been sent."
    else
      flash[:alert] = "Missing Required Field."
      render :new
    end
  end

private

  def contact_params
    params.require(:contact).permit(:name, :email, :reason, :content)
  end

end
