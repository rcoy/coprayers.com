class ApplicationController < ActionController::Base

	helper_method :request_count
  helper_method :unread_count

  protect_from_forgery with: :exception

  def current_controller?(names)
  	names.include?(params[:controller]) unless params[:controller].blank? || false
 	end

 	helper_method :current_controller?

  def request_count
  	current_user.pending_friend_requests_from.map(&:user).size
  end

  def unread_count
  # get all the converastion that I'm involved
    Message.joins(:conversation).where(
      "(conversations.sender_id = ? OR conversations.recipient_id = ?) AND (messages.read = ? AND messages.user_id != ?)", current_user, current_user, false, current_user).count
  end

end
