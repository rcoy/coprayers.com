class ConversationsController < ApplicationController

 before_action :authenticate_user!
 before_action :set_conversation, only: [:destroy]

def index
 @user = current_user
 @advertisements = Advertisement.limit(3).order("RANDOM()")
 @conversations = Conversation.where("sender_id = ? OR recipient_id = ?", @user.id, @user.id)
 end

def create
 if Conversation.between(params[:sender_id],params[:recipient_id])
   .present?
    @conversation = Conversation.between(params[:sender_id],
     params[:recipient_id]).first
 else
  @conversation = Conversation.create!(conversation_params)
 end

 redirect_to conversation_messages_path(@conversation)

end

def destroy
	@conversation.destroy
	respond_to do |format|
		format.html {redirect_to conversations_path, notice: "Conversation Deleted"}
	end
end

private
 def conversation_params
  params.permit(:sender_id, :recipient_id)
 end

 def set_conversation
 	@conversation = Conversation.find(params[:id])
 end

end
