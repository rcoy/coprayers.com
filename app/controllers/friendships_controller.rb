class FriendshipsController < ApplicationController

	before_action :authenticate_user!
	before_action :set_user, only: [:create]
	before_action :set_friendship, only: [:destroy, :accept]

	def create
		@friendship = current_user.request_friendship(@user)
		respond_to do |format|

			get_counts()
			if @user.email_notify_friend_received
				ApplicationMailer.delay.friendship_requested(current_user,@user)
			end

			format.html {redirect_to users_path, notice: "Friendship Requested"}
			format.js
		end
	end

	def destroy
		@user = @friendship.user == current_user ? @friendship.friend : @friendship.user

		Friendship.transaction do
			@friendship.destroy
			a = Subscription.where(user_id: @friendship.friend, subscribe_to_id: @friendship.user )
			a.first.destroy unless a.first.nil?
			b = Subscription.where(user_id: @friendship.user, subscribe_to_id: @friendship.friend )
			b.first.destroy unless b.first.nil?
		end

		respond_to do |format|
			format.html {redirect_to users_path, notice: "Friendship Deleted"}
			format.js
		end
	end

	def accept
		@user = @friendship.user == current_user ? @friendship.friend : @friendship.user
		@friendship.accept_friendship
		respond_to do |format|

			if @user.email_notify_friend_approved
				ApplicationMailer.delay.friendship_accepted(current_user,@user)
			end

			format.html {redirect_to users_path, notice: "Friendship Accepted"}
			format.js
		end
	end

	private

	def set_user
		@user = User.find(params[:user_id])
	end

	def set_friendship
		@friendship = Friendship.find(params[:id])
	end

	def get_counts
    @friend_count = current_user.active_friends.count
    @pending_count = current_user.pending_friend_requests_to.map(&:friend).size
  end

end
