class UsersController < ApplicationController

	before_action :authenticate_user!, only: [:index, :upload, :crop]
	before_action :set_user, only:[:show, :friends, :lock_user, :unlock_user]
  before_action :get_counts, only: [:index]

	def index
      @invite = Invite.new
      @advertisements = Advertisement.limit(3).order("RANDOM()")
		case params[:people] when "friends"
			@users = Kaminari.paginate_array(current_user.active_friends).page(params[:page]).per(10)
		when "requests"
			@users_request = current_user.pending_friend_requests_from.map(&:user)
      @users_pending = current_user.pending_friend_requests_to.map(&:friend)
		else
      @users = User.text_search(params[:query]).order("created_at DESC").where.not(id: current_user.id).page(params[:page]).per(10)
		end
	end

  def show
  	@post = Post.new
    @advertisements = Advertisement.limit(3).order("RANDOM()")
    @posts = @user.posts.order('created_at DESC').page(params[:page]).per(10)
  end

  def upload
  	@user = current_user
  end

	def crop
  	@user = current_user
	end

  def friends
    @users = @user.active_friends
  end

  def remove
    @user = current_user
    @user.remove_avatar!
    @user.save
    redirect_to edit_user_registration_path, notice: 'Photo Sucessfully Removed' 
  end

  # Used to update user avatar
  def update
    @user = current_user
      respond_to do |format|
        if params[:user] && @user.update(user_params)
          format.html {
            if params[:user][:avatar].present?
              redirect_to users_crop_path
            else
              # Delete Large upload after User makes crop selection 
              File.delete @user.avatar.path if File.exists?(@user.avatar.path)
              redirect_to edit_user_registration_path, notice: 'User was successfully updated.' 
            end
            }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { redirect_to users_upload_path }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

  def lock_user
    @user.lock_user  
    redirect_to user_path(@user.username)
  end


  def unlock_user
    @user.unlock_user
    redirect_to user_path(@user.username)
  end

  private

  def set_user
  	@user = User.find_by(username: params[:id])
  end

  def get_counts
    @friend_count = current_user.active_friends.count
    @pending_count = current_user.pending_friend_requests_to.includes(:friend).map(&:friend).size
  end

  def user_params
    params.require(:user).permit(:name, :avatar, :crop_x, :crop_y, :crop_w, :crop_h)
  end
  
end
