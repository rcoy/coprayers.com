class GroupsController < ApplicationController

  before_filter :authenticate_user!
  before_filter :evaluate_role, only: [:members]
  before_filter :set_group, only: [:edit, :upload, :show, :members, :update, :crop, :image_remove, :destroy]

  def index
    @groups = Group.text_search(params[:query]).order("created_at DESC").page(params[:page]).per(10)
    @advertisements = Advertisement.limit(3).order("RANDOM()")
    @count = current_user.group_members.where(role: "admin").count + current_user.group_members.where(role: "moderator").count + current_user.group_members.where(role: "member").count
    @member_level = current_user.subscription_plan
  end 

  def new
    @group = Group.new
    @user = current_user
    @count = current_user.group_members.where(role: "admin").count + current_user.group_members.where(role: "moderator").count
  end

  def create
    @count = current_user.group_members.where(role: "admin").count + current_user.group_members.where(role: "moderator").count
    @group = current_user.groups.build(group_params)
    if @group.save
      gm = GroupMember.new
      gm.user_id = current_user.id
      gm.group = @group
      gm.role = "admin"
      gm.save
      redirect_to groups_path
    else
      render 'new'
    end
  end

  def edit
    if current_user.admin_this_group?(@group)
      
    else 
      redirect_to groups_path
    end
  end

  def upload

  end

  def show
    @group_invite = GroupInvite.new
    @group_post = GroupPost.new
    @group_posts = @group.group_posts.order('created_at DESC').page(params[:page]).per(10)
    @members = @group.group_members.where(role: "member")
    @owners = @group.group_members.where(role: "admin")
    @advertisements = Advertisement.limit(3).order("RANDOM()")
    if current_user
      @current_member = current_user.group_members.find_by(group_id: @group.id)
    end

  end

  # Possible roles in decending order: admin, moderator, member, pending, blocked

  def your_groups
    @advertisements = Advertisement.limit(3).order("RANDOM()")
    @admin = current_user.group_members.where(role: "admin")
    @moderator = current_user.group_members.where(role: "moderator")
    @member = current_user.group_members.where(role: "member")
    @requests = current_user.group_members.where(role: "pending")
  end

  def members
    @admins = @group.group_members.where(role: 'admin')
    @moderators = @group.group_members.where(role: 'moderator')
    @pending = @group.group_members.where(role: 'pending')
    @members = @group.group_members.where(role: 'member')
    
    if current_user
      @current_member = current_user.group_members.find_by(group_id: @group.id)
    end

  end

  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html {
          if params[:group][:image].present?
            redirect_to group_crop_path(@group)
          else
            # Delete Large upload after User makes crop selection 
              File.delete @group.image.path if File.exists?(@group.image.path)
            redirect_to edit_group_path, notice: "Group was successfully updated."
          end
        }
        format.json { render :show, status: :ok, location: @group }
    else
      format.html { redirect_to group_upload_path }
      format.json { render json: @user.errors, status: :unprocessable_entity }
    end
  end
end

  def crop

  end

  def image_remove
    @group.remove_image!
    @group.save
    redirect_to edit_group_path, notice: "Image was successfully removed."
  end

  def destroy
    @group.destroy
    redirect_to groups_path, notice: "Group Deleted."
  end

  private

  def group_params
    params.require(:group).permit(:name, :organization, :location, :description, :privacy, :image, :crop_x, :crop_y, :crop_w, :crop_h)
  end

  def set_group
    @group = Group.find(params[:id])
  end

  def evaluate_role
    gm = GroupMember.find_by(user_id: current_user.id,  group_id: params[:id])
    if gm == nil || gm.role == 'pending' || gm.role == 'blocked'
      redirect_to group_path(params[:id])
    end
  end

end
