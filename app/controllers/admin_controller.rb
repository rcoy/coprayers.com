class AdminController < ApplicationController

	before_action :authenticate_user!
	before_filter :verify_is_admin

	before_action :set_user
	
	def index
		# Totals
		@user_count = User.count
		@group_count = Group.count
		@post_count = Post.count + GroupPost.count

		# New Users
		@new_hour = User.where(created_at: 1.hour.ago..DateTime.now).count
		@new_day = User.where(created_at: 1.day.ago..DateTime.now).count
		@new_month = User.where(created_at: 1.month.ago..DateTime.now).count
		@new_year = User.where(created_at: 1.year.ago..DateTime.now).count


		# Paying Members
		@monthly_individuals = User.where(subscription_plan: "Individual_Monthly").count
		@annual_individuals = User.where(subscription_plan: "Individual_Annual").count
		@monthly_organizations = User.where(subscription_plan: "Organization_Monthly").count
		@annual_organizations = User.where(subscription_plan: "Organization_Annual").count
		
		# Active Users
		@hour = User.where(current_sign_in_at: 1.hour.ago..DateTime.now).count
		@day = User.where(current_sign_in_at: 1.day.ago..DateTime.now).count
		@month = User.where(current_sign_in_at: 1.month.ago..DateTime.now).count
		@year = User.where(current_sign_in_at: 1.year.ago..DateTime.now).count
		
		# Groups
		@group_count = Group.count
		@new_groups_hour = Group.where(created_at: 1.hour.ago..DateTime.now).count
		@new_groups_day = Group.where(created_at: 1.day.ago..DateTime.now).count
		@new_groups_month = Group.where(created_at: 1.month.ago..DateTime.now).count
		@new_groups_year = Group.where(created_at: 1.year.ago..DateTime.now).count
		
		# Posts
		@post_count = Post.count + GroupPost.count
		@new_posts_hour = Post.where(created_at: 1.hour.ago..DateTime.now).count + GroupPost.where(created_at: 1.hour.ago..DateTime.now).count
		@new_posts_day = Post.where(created_at: 1.day.ago..DateTime.now).count + GroupPost.where(created_at: 1.day.ago..DateTime.now).count
		@new_posts_month = Post.where(created_at: 1.month.ago..DateTime.now).count + GroupPost.where(created_at: 1.month.ago..DateTime.now).count
		@new_posts_year = Post.where(created_at: 1.year.ago..DateTime.now).count + GroupPost.where(created_at: 1.month.ago..DateTime.now).count
		
		# Prayed For
		@prayed_count = Vote.count
		@prayed_hour = Vote.where(created_at: 1.hour.ago..DateTime.now).count
		@prayed_day = Vote.where(created_at: 1.day.ago..DateTime.now).count
		@prayed_month = Vote.where(created_at: 1.month.ago..DateTime.now).count
		@prayed_year = Vote.where(created_at: 1.year.ago..DateTime.now).count

	end

	def users
		@users = User.all.order('created_at DESC').where.not(id: current_user.id).page(params[:page]).per(10)
	end

	def groups
		@groups = Group.all.page(params[:page]).per(10)
	end

	def ads
		@advertisements = Advertisement.all.page(params[:page]).per(10)
	end

private

	def set_user
		@user = current_user
	end

	def verify_is_admin
  	(current_user.nil?) ? redirect_to(root_path) : (redirect_to(root_path) unless current_user.admin?)
	end

end
