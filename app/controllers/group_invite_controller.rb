class GroupInviteController < ApplicationController

  before_action :set_group

  def new
    @group_invite = GroupInvite.new
  end

  def create
    @group_invite = GroupInvite.new(group_invite_params)
    @current_user = current_user
    if @group_invite.valid?
      ApplicationMailer.delay.new_group_invite(current_user, @group_invite, @group)
      flash[:notice] = "Your invitation has been sent."
      redirect_to(:back)
    else
      flash[:alert] = "An error occurred while delivering this invitation."
      redirect_to(:back)
    end
  end

private

  def group_invite_params
    params.require(:group_invite).permit(:email, :first_name)
  end

  def set_group
    @group = Group.find(params[:id])
  end

end
