class InviteController < ApplicationController

  def new
    @invite = Invite.new
  end

  def create
    @invite = Invite.new(invite_params)
    @current_user = current_user
    if @invite.valid?
      ApplicationMailer.delay.new_invite(current_user,@invite)
      redirect_to users_path, notice: "Your invitation has been sent."
    else
      flash[:alert] = "An error occurred while delivering this invitation."
      redirect_to users_path
    end
  end

private

  def invite_params
    params.require(:invite).permit(:email, :first_name)
  end

end
