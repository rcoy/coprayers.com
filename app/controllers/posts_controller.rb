class PostsController < ApplicationController

before_action :authenticate_user!, only: [:index, :upvote]
before_action :set_post, only: [:edit, :update, :destroy, :upvote]

def create
	@post = current_user.posts.new(post_params)
	if @post.save

		@subscribers = current_user.subscribers
			@subscribers.each do |f|
				ApplicationMailer.delay.send_prayer_request(@post, f)
			end
		
		respond_to do |format|
			format.html {redirect_to activities_path, notice: "Post Created"}
		end
	else
		redirect_to user_path(@post.user.username), notice: "Something went wrong"
	end
end

def edit
	@user = current_user
end

def update
	if @post.update(post_params)
		respond_to do |format|
			format.html {redirect_to activities_path(@post.user.username), notice: "Post Updated"}
		end
	else
		redirect_to post_path(@post), notice: "Something went wrong"
	end
end

def destroy
	@post.destroy
	respond_to do |format|
		format.html {redirect_to activities_path, notice: "Post Deleted"}
	end
end

def upvote
	respond_to do |format|
		unless current_user.voted_for? @post 
			format.html { redirect_to :back }
			format.json { head :no_content }
			format.js { render :layout => false }
			@post.upvote_from current_user
		else
			format.html { redirect_to :back }
			format.json { head :no_content }
			format.js
		end
	end
end

private

def set_post
	@post = Post.find(params[:id])
end

def post_params
	params.require(:post).permit(:post_type, :content)
end

end
