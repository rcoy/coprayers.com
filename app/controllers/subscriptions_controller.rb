class SubscriptionsController < ApplicationController

	before_action :authenticate_user!
	before_action :set_user, only: [:create]
	before_action :set_subscription, only: [:destroy]

	def create
		@subscription = current_user.subscribe_to(@user)
		respond_to do |format|
			format.html {redirect_to users_path, notice: "Email Notification Turned On."}
			format.js
		end
	end

	def destroy
		@user = @subscription.subscribe_to
		@subscription.destroy
		respond_to do |format|
			format.html {redirect_to users_path, notice: "Email Notification Turned Off."}
			format.js
		end
	end

private

	def set_user
		@user = User.find(params[:subscribe_to_id])
	end

	def set_subscription
		@subscription = Subscription.find(params[:id])
	end

end
