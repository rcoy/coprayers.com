class GroupMembersController < ApplicationController

  before_action :authenticate_user!
	before_action :set_group
  before_action :set_membership, only: [:make_member, :make_moderator, :make_admin, :reject_member, :email_notify_on, :email_notify_off]

 def create
    # Test for group being joined public or private
    # If Private
    if @group.private?
      @group.group_members.create!(user: current_user, role: "pending")
      
      @group.admins.each do |owner|
        ApplicationMailer.delay.join_requested(current_user, owner, @group)
      end

      redirect_to @group, notice: "Join Request Sent!"
    else
      # If Public
      @group.group_members.create!(user: current_user, role: "member")
      redirect_to @group, notice: "Thanks for Joining!"
    end
  end

  def make_member
    @membership.make_member
    ApplicationMailer.delay.join_accepted(current_user, @membership.user, @group)
    redirect_to group_members_path(@group), notice: "User was made a group member"
  end

  def reject_member
    @membership.reject_member
    redirect_to group_members_path(@group), notice: "Request Rejected"
  end

  def ban_member
    @member = User.find(params[:user_id])
    membership = GroupMember.find_by(user_id: params[:user_id], group: @group)
    GroupMember.transaction do
      membership.remove_member
      GroupPost.where(user_id: membership.user_id, group: @group).delete_all
    end
    redirect_to @group, notice: "Member Removed"
  end

  def make_admin
    @membership.make_admin
    ApplicationMailer.delay.make_member_admin(current_user, @membership.user, @group)
    redirect_to group_members_path(@group), notice: "User was made group administrator"
  end

  def make_moderator
    @membership.make_moderator
    ApplicationMailer.delay.make_member_moderator(current_user, @membership.user, @group)
    redirect_to group_members_path(@group), notice: "User was made group moderator"
  end

  def destroy
  	member = current_user.group_members.find(params[:id])
  	member.destroy
  	redirect_to @group, notice: "You Left The Group"
  end

  def email_notify_on
    @membership.email_notify_on
    redirect_to @group, notice: "Email Notify Turned On"
  end

  def email_notify_off
    @membership.email_notify_off
    redirect_to @group, notice: "Email Notify Turned Off"
  end

private

  def set_group
  	@group = Group.find(params[:group_id])
  end

  def set_membership
    @membership = GroupMember.find(params[:request_id])
  end

end
