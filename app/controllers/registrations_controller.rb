class RegistrationsController < Devise::RegistrationsController


protected

	def sign_up_params
		params.require(:user).permit(:first_name, :last_name, :username, :email, :password)
	end

	def account_update_params
		params.require(:user).permit(:email, :password, :current_password, :username, :first_name, :last_name, :city, :state, :bio, :avatar, :avatar_cache, :remove_avatar, :email_notify_friend_received, :email_notify_friend_approved, :email_notify_private_message, :private_message_system_on)
	end

	def after_update_path_for(resource)
    edit_user_registration_path
  end

	def after_inactive_sign_up_path_for(resource)
		welcome_path
	end

end
