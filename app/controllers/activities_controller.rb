class ActivitiesController < ApplicationController

	before_action :authenticate_user!

	def index
		@post = Post.new
		@user = current_user
		@users = current_user.active_friends
		@users.push(current_user)
		@advertisements = Advertisement.limit(3).order("RANDOM()")
		@groups = current_user.group_members.where.not(role: 'pending').limit(5)
		@activities = Kaminari.paginate_array(Post.where(user_id: @users).includes(:user).order('created_at DESC')).page(params[:page]).per(10)
	end

end
