class ChargesController < ApplicationController

	before_filter :authenticate_user!

	def create_individual
		customer = Stripe::Customer.create(
			:email 	=> current_user.email,
			:card		=> params[:stripeToken1],
			:plan		=> "CS250PM"
			)

		current_user.subscribed = true
		current_user.subscription_plan = "Individual_Monthly"
		current_user.stripe_id = customer.id
		current_user.save

		redirect_to support_path

		rescue Stripe::CardError => e
	 		flash[:error] = e.message
	 		redirect_to support_path
	end


	def create_individual_annual
		customer = Stripe::Customer.create(
			:email 	=> current_user.email,
			:card		=> params[:stripeToken2],
			:plan		=> "ACS19PY"
			)

		current_user.subscribed = true
		current_user.subscription_plan = "Individual_Annual"
		current_user.stripe_id = customer.id
		current_user.save

		redirect_to support_path

		rescue Stripe::CardError => e
	 		flash[:error] = e.message
	 		redirect_to support_path
	end

	def create_organization
		customer = Stripe::Customer.create(
			:email 	=> current_user.email,
			:card		=> params[:stripeToken3],
			:plan		=> "OCS900PM"
			)

		current_user.subscribed = true
		current_user.subscription_plan = "Organization_Monthly"
		current_user.stripe_id = customer.id
		current_user.save

		redirect_to support_path

		rescue Stripe::CardError => e
	 		flash[:error] = e.message
	 		redirect_to support_path
	end

	def create_organization_annual
		customer = Stripe::Customer.create(
			:email 	=> current_user.email,
			:card		=> params[:stripeToken4],
			:plan		=> "AOCS89PY"
			)

		current_user.subscribed = true
		current_user.subscription_plan = "Organization_Annual"
		current_user.stripe_id = customer.id
		current_user.save

		redirect_to support_path

		rescue Stripe::CardError => e
	 		flash[:error] = e.message
	 		redirect_to support_path
	end

	def cancel_subscription
		customer = Stripe::Customer.retrieve(current_user.stripe_id)
		customer.cancel_subscription

		current_user.subscribed = false
		current_user.subscription_plan = ""
		current_user.stripe_id = ""
		current_user.save

		flash[:notice] = "Canceled subscription."
		redirect_to support_path

    rescue Stripe::InvalidRequestError => e
      logger.error "Stripe error while deleting customer: #{e.message}"
      errors.add :base, "No active subscriptions for user."
     false
	end

end
