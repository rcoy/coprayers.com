class MessagesController < ApplicationController

  before_action do
    @conversation = Conversation.find(params[:conversation_id])
    if !(@conversation.sender_id == current_user.id || @conversation.recipient_id == current_user.id)
      redirect_to root_path
    end
  end

def index
 @user = current_user
 @groups = current_user.group_members.where.not(role: 'pending').limit(5)
 @advertisements = Advertisement.limit(3).order("RANDOM()")
 @conversations = Conversation.where("sender_id = ? OR recipient_id = ?", @user.id, @user.id)
 @messages = Kaminari.paginate_array(@conversation.messages.order('created_at DESC')).page(params[:page]).per(10)
 
 if @conversation.sender_id == current_user.id
   @other_member = @conversation.recipient
 else
   @other_member = @conversation.sender
 end

 #if @messages.length > 10
 #  @over_ten = true
 #  @messages = @messages[-10..-1]
 #end
 #
 #if params[:m]
 #  @over_ten = false
 #  @messages = @conversation.messages.order('created_at DESC')
 #end

 # NOTE: this can be slow in the future ....
    @messages.each do |message|
      if message.user_id != current_user.id
        message.update_attribute(:read, true);
      end
  end

  @message = @conversation.messages.new
 
 end

 def new
  @message = @conversation.messages.new
 end

def create
 @message = @conversation.messages.new(message_params)
 if @message.save

    if @conversation.sender_id == current_user.id
      @other_member = @conversation.recipient
    else
      @other_member = @conversation.sender
    end

    if @other_member.email_notify_private_message
      ApplicationMailer.delay.new_private_message(current_user, @other_member, @message)
    end

    redirect_to conversation_messages_path(@conversation), notice: "Message Added"
 else
    redirect_to conversation_messages_path(@conversation), alert: "A Message is required"

 end
end

private
 def message_params
  params.require(:message).permit(:body, :user_id)
 end
end
