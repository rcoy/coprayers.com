class StaticController < ApplicationController
  
  before_filter :authenticate_user!, only: [:support]

  def home
    if current_user
      if current_user.status == "active"
        @user = current_user
        redirect_to activities_path
      else
        flash[:error] = "Your account is expired. Please contact Support for renewal."
        redirect_to destroy_user_session_path, method: delete
      end
    end
    # Used to display last 16 members on home page 
    @users = User.where.not(avatar: nil).reverse.last(16)

  end

  def faq
  end

  def contact
  end

  def terms
  end

  def privacy
  end

  def advertise
  end

  def support
    @user = current_user
  end

end
