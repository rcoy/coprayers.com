class GroupPostsController < ApplicationController

	before_action :authenticate_user!
	before_action :set_group_post, only: [:edit, :update, :destroy, :upvote]

	def create
		@group = Group.find(params[:group_id])
		
		@group_post = current_user.group_posts.new(group_post_params)
		
		@group_post.group = @group

		if @group_post.save

			users = @group.group_members.where(email_notify: true).includes(:user)

			users.each do |user|
				ApplicationMailer.delay.send_group_prayer_request(current_user, @group_post, user.user) unless user.id == current_user.id
			end

			respond_to do |format|
				format.html {redirect_to group_path(@group), notice: "Group Post Created!"}
			end
		else
			redirect_to group_path(@group), notice: "You need to enter some text."
		end
	end

	def edit
	end

	def update
		if @group_post.update(group_post_params)
			respond_to do |format|
				format.html {redirect_to group_path(@group_post.group_id), notice: "Group Post Updated."}
			end
		else
			redirect_to post_path(@group_post), notice: "Something went wrong."
		end
	end

	def destroy
		@group_post.destroy
		respond_to do |format|
				format.html {redirect_to group_path(@group_post.group_id), notice: "Group Post Removed"}
			end
	end

	def upvote
	respond_to do |format|
		unless current_user.voted_for? @group_post 
			format.html { redirect_to :back }
			format.json { head :no_content }
			format.js { render :layout => false }
			@group_post.upvote_from current_user
		else
			format.html { redirect_to :back }
			format.json { head :no_content }
			format.js
		end
	end
end

	private

	def group_post_params
		params.require(:group_post).permit(:content, :post_type, :group_id)
	end

	def set_group_post
		@group_post = GroupPost.find(params[:id])
	end

end
