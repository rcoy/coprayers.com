# encoding: utf-8
class GroupUploader < CarrierWave::Uploader::Base
 include CarrierWave::RMagick

  # storage :file
  storage :fog

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Only allows jpg, jpeg, or png
  def extension_white_list
    %w(jpg jpeg png gif)
  end
 
  resize_to_limit(600, 600)

  version :profile do
    process :crop
    resize_to_fill(72, 72)
  end

  version :friendship do
    process :crop
    resize_to_fill(48, 48)
  end

  version :navigation do
    process :crop
    resize_to_fill(20, 20)
  end
  
  def crop
    if model.crop_x.present?
      resize_to_limit(600, 600)
      manipulate! do |img|
        x = model.crop_x.to_i
        y = model.crop_y.to_i
        w = model.crop_w.to_i
        h = model.crop_h.to_i
        img.crop!(x, y, w, h)
      end
    end
  end

end
