Rails.application.routes.draw do

  devise_for :users, controllers: {registrations: 'registrations'}
  
  root 'static#home'
	
	get 'users/upload'      => 'users#upload'
  post 'users/crop'       => 'users#crop'
  get 'users/crop'        => 'users#crop'
  get 'users/remove'      => 'users#remove'
  get 'users/:id/friends', to: 'users#friends', as: 'friends'
	
	resources :users do 
		member do
			get 'lock_user'
			get 'unlock_user'
		end
	end
	
	resources :friendships, only: [:create, :destroy, :accept] do
		member do
			put :accept
		end
	end

	resources :posts, only: [:create, :edit, :update, :destroy] do
		member do
			put "prayed" => "posts#upvote"
		end
	end

	resources :group_posts, only: [:create, :edit, :update, :destroy] do
		member do
			put "prayed" => "group_posts#upvote"
		end
	end

	get 	'groups/:id/upload'				=> 'groups#upload', as: 'group_upload'
  post  'groups/:id/crop'   			=> 'groups#crop', as: 'group_crop'
  get		'groups/:id/crop'					=> 'groups#crop'
  get		'groups/:id/image_remove'	=> 'groups#image_remove', as: 'group_image_remove'

	get '/groups/your_groups' => 'groups#your_groups'

  get '/groups/:id/members/', to: 'groups#members', as: 'group_members'
  put '/groups/:group_id/members/make_admin/:request_id', to: 'group_members#make_admin', as: 'make_admin'
  put '/groups/:group_id/members/make_member/:request_id', to: 'group_members#make_member', as: 'make_member'
  put '/groups/:group_id/members/make_moderator/:request_id', to: 'group_members#make_moderator', as: 'make_moderator'
  delete '/groups/:group_id/members/reject/:request_id', to: 'group_members#reject_member', as: 'reject_member'
  delete '/groups/:group_id/members/ban/:user_id', to: 'group_members#ban_member', as: 'ban_member'

  put '/groups/:group_id/members/email_notify_on/:request_id', to: 'group_members#email_notify_on', as: 'email_notify_on'
  put '/groups/:group_id/members/email_notify_off/:request_id', to: 'group_members#email_notify_off', as: 'email_notify_off'


  resources :groups do
    resources :group_posts
    resources :group_members
  end

  resources :conversations do
  	resources :messages
 	end

	resources :subscriptions, only: [:create, :destroy]

	resources :activities, only: [:index]

	# Used for Stripe Subscriptions
	post '/charge/individual', to: 'charges#create_individual'
	post '/charge/individual_annual', to: 'charges#create_individual_annual'
	post '/charge/organization', to: 'charges#create_organization'
	post '/charge/organization_annual', to: 'charges#create_organization_annual'
	delete '/charge/cancel', to: 'charges#cancel_subscription'

	get 'contact', 				to: 'contact#new', as: 'contact'
	post 'contact', 			to: 'contact#create'

	post 'invite', 				to: 'invite#create'
	post 'group_invite', 	to: 'group_invite#create'

	#Admin Pages
	get 'admin'					=> 'admin#index'
	get 'admin/users'		=> 'admin#users'
	get 'admin/groups'	=> 'admin#groups'
	get 'admin/ads'			=> 'admin#ads'
	
	resources :advertisements

  # Static Pages
  get 'privacy' 	=> 'static#privacy'
  get 'terms'   	=> 'static#terms'
 	get 'faq' 			=> 'static#faq'
 	get 'advertise' => 'static#advertise'
 	get 'blocked' 	=> 'static#blocked'
 	get 'support' 	=> 'static#support'
 	get 'welcome' 	=> 'static#welcome'

 	get "*any", via: :all, to: "errors#not_found"

end
